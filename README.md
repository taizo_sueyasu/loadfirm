#LoadFirm
====

LoadFirm is a Linux kernel module to load external firmware.

## Build

`$ make`

## Usage

`$ sudo dd if=/dev/urandom of=/lib/firmware/sample.bin bs=4k count=1`

`$ sudo insmod ./loadfirm.ko`

## License

GPL
