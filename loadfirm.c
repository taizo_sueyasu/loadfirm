#include <linux/init.h>
#include <linux/module.h>
#include <linux/firmware.h>
#include <linux/platform_device.h>

#define FIRMWARE "sample.bin"

struct platform_device *pdev;
const struct firmware *fw;

static int __init loadfirm_init(void)
{
	pdev = platform_device_register_simple("loadfirm", 0, NULL, 0);
	if (IS_ERR(pdev)) PTR_ERR(pdev);
	if(request_firmware(&fw, FIRMWARE, &pdev->dev)) {
		platform_device_unregister(pdev);
		return -EINVAL;
	}
	release_firmware(fw);
	return 0;
}

 static __exit void loadfirm_exit(void)
{
	platform_device_unregister(pdev);
}

module_init(loadfirm_init);
module_exit(loadfirm_exit);

MODULE_FIRMWARE(FIRMWARE);
MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("load firmware sample");